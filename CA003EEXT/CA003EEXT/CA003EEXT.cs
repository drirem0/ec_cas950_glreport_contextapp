﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Mongoose.IDO;
using Mongoose.IDO.Protocol;
using Mongoose.IDO.DataAccess;
using Mongoose.Core.Common;

namespace CA003EEXT
{
    public class CA003EEXT : IDOExtensionClass
    {

        public override void SetContext(IIDOExtensionClassContext context)
        {
            base.SetContext(context);
            Context.IDO.PostLoadCollection += new IDOEventHandler(IDO_PostLoadCollection);
        }

        void IDO_PostLoadCollection(object sender, IDOEventArgs args)
        {
            LoadCollectionResponseData responseData = args.ResponsePayload as LoadCollectionResponseData;

        }


        public IDataReader App01(String Input1, String Input2, String Input3, String Input4, int Input5, String Input6, int cono, String divi)

        {
            DataTable results = new DataTable("CA002");





            results.Columns.Add("IBPUNO");
            results.Columns.Add("IBPNLI");
            results.Columns.Add("IBSUNO");
            results.Columns.Add("IDSUNM");
            results.Columns.Add("EZACAM");
            results.Columns.Add("IBORQA");
            results.Columns.Add("IBPITD");
            results.Columns.Add("IBITNO");


            DataRow newRow = results.NewRow();

            using (ApplicationDB db = this.CreateApplicationDB())
            {


                IDbCommand sqlCommand = db.CreateCommand();

                sqlCommand.CommandText = string.Format("select EZFSRC, T01.IBPUNO, T01.IBPNLI, T05.IDSUNO, T05.IDSUNM, T01.IBORQA, T01.IBPITD, T01.IBITNO, T02.IBPUNO, T02.IBPNLI, T03.IDSUNO, T03.IDSUNM, T02.IBORQA, T02.IBPITD, T02.IBITNO, EZACAM FROM M3CINACC " +
                    " left join M3MITTRA on EZCONO = MTCONO and EZANBR = MTANBR  left join M3MPLINE T01 on MTCONO = T01.IBCONO and MTRIDN = T01.IBPUNO and MTRIDL = T01.IBPNLI left join M3CIDMAS T05 on T01.IBCONO = T05.IDCONO and T01.IBSUNO = T05.IDSUNO " +
                    " left join CRACTR on EZCONO = EYCONO and EZANBR = EYANBR left join M3MPLINE T02 on EYCONO = T02.IBCONO and EYTRNR = T02.IBPUNO and EYPONR = T02.IBPNLI and EYPOSX = T02.IBPNLS left join M3CIDMAS T03 on T02.IBCONO = T03.IDCONO and T02.IBSUNO = T03.IDSUNO " +
                    " where EZCONO = " + cono + " and EZDIVI = '" + divi + "' and EZVONO = " + Input5 + " and EZAIT1 = '" + Input1 + "' and ISNULL(EZAIT3,'') = '" + Input2 + "' and ISNULL(EZAIT5,'') = '" + Input3 + "' and ISNULL(EZAIT6,'') = '" + Input4 + "' and ISNULL(EZAIT7,'') = '" + Input6 + "'");
                sqlCommand.CommandType = System.Data.CommandType.Text;

                IDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())

                {

                    string FSRC = dr[0].ToString();
                    string PUNO = dr[1].ToString();
                    string PNLI = dr[2].ToString();
                    string SUNO = dr[3].ToString();
                    string SUNM = dr[4].ToString();
                    string ORQA = dr[5].ToString();
                    string PITD = dr[6].ToString();
                    string ITNO = dr[7].ToString();

                    string PUNO2 = dr[8].ToString();
                    string PNLI2 = dr[9].ToString();
                    string SUNO2 = dr[10].ToString();
                    string SUNM2 = dr[11].ToString();
                    string ORQA2 = dr[12].ToString();
                    string PITD2 = dr[13].ToString();
                    string ITNO2 = dr[14].ToString();

                    string ACAM = dr[15].ToString();



                    if (FSRC == "1")
                    {
                        newRow["IBPUNO"] = PUNO;
                        newRow["IBPNLI"] = PNLI;
                        newRow["IBSUNO"] = SUNO;
                        newRow["IDSUNM"] = SUNM;
                        newRow["IBORQA"] = ORQA;
                        newRow["IBPITD"] = PITD;
                        newRow["IBITNO"] = ITNO;


                    }

                    if (FSRC == "3")
                    {
                        newRow["IBPUNO"] = PUNO2;
                        newRow["IBPNLI"] = PNLI2;
                        newRow["IBSUNO"] = SUNO2;
                        newRow["IDSUNM"] = SUNM2;
                        newRow["IBORQA"] = ORQA2;
                        newRow["IBPITD"] = PITD2;
                        newRow["IBITNO"] = ITNO2;

                    }


                    newRow["EZACAM"] = ACAM;





                    results.Rows.Add(newRow);
                    newRow = results.NewRow();

                }




                return results.CreateDataReader();

            }



        }









    }


}

