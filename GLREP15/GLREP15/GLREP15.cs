﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web;
using System.Data;
using Mongoose.IDO;
using Mongoose.IDO.Protocol;
using Mongoose.IDO.DataAccess;
using Mongoose.Core.Common;


namespace GLREP15
{
    public class GLREP15 : IDOExtensionClass
    {


        decimal acam003;
        string acam003S;

        decimal egait1rd;
        decimal egait3rd;

        string duprow;
        string egcono;
        string egdivi;
        string egjrno;
        string egacdt;
        string egait1;
        string egait2;
        string egait3;
        string egait4;
        string egait5;
        string egait6;
        string egait7;
        string egacam;
        string egvtxt;
        string egait1s;
        string egait3s;
        string egait1r;
        string egait1rm;
        string egait3r;
        string egait3rm;
        string ezanbr;
        string ezacam;
        string ezfsrc;
        string t04puno;
        string t04pnli;
        string t04suno;
        string t04sunm;
        string t04orqa;
        string t04pitd;
        string t04itno;

        string egpuno;
        string egpnli;
        string egpunosav;
        string egpnlisav;


        string t07puno;
        string t07pnli;
        string t07suno;
        string t07sunm;
        string t07orqa;
        string t07pitd;
        string t07itno;
        string egjsno;

        string t10punosav;
        string t10pnlisav;

        string egvdsc;

        string t10puno;
        string t10pnli;
        string t10suno;
        string t10sunm;

        string t10orqa;
        string t10pitd;
        string t10itno;

        string egtx40;
        decimal egtxy1;
        decimal egtxy2;

        string ezacamc;

        string egait1sav = "";
        string egait2sav = "";
        string egait3sav = "";
        string egait5sav = "";
        string egait6sav = "";
        string egait7sav = "";
        string egacamsav = "";
        string ezanbrsav = "";



        string egait1rx;
        string egait3rx;

        string datefrom;
        string dateto;

        string SQL01;

        string SQLV;

        string fromyear;
        string frommonth;
        string fromday;

        string toyear;
        string tomonth;
        string today;

        string tcono;
        string tdivi;
        string tacam;
        string tjrno;
        string tjsno;
        string tvtxt;

        string casen;
        string dupchk;

        string fromdate01;
        string todate01;

        string totalplu;

        string tjrnosav;
        string tjsnosav;



        public override void SetContext(IIDOExtensionClassContext context)
        {
            base.SetContext(context);
            Context.IDO.PostLoadCollection += new IDOEventHandler(IDO_PostLoadCollection);
        }

        void IDO_PostLoadCollection(object sender, IDOEventArgs args)
        {
            LoadCollectionResponseData responseData = args.ResponsePayload as LoadCollectionResponseData;

        }


        public IDataReader LoadReport(string cono, string divi,
                                      string ait301, string ait302, string ait303, string ait304,
                                      string ait305, string ait306, string ait307, string ait308,
                                      string ait309, string ait310, string ait311, string ait312,
                                      string ait313, string ait314, string ait315, string ait316,
                                      string ait101, string ait102, string ait103, string ait104,
                                      string ait105, string ait106, string ait107, string ait108,
                                      string ait109, string ait110, string ait111, string ait112,
                                      string ait113, string ait114, string ait115, string ait116,
                                      string ait1F, string ait1T, string ait3F, string ait3T, string datefrom, string dateto)





        {
            DataTable results = new DataTable("GLReport");

            results.Columns.Add("EGCONO");
            results.Columns.Add("EGDIVI");
            results.Columns.Add("EGJRNO");
            results.Columns.Add("EGACDT");
            results.Columns.Add("EGAIT1");
            results.Columns.Add("EGAIT2");
            results.Columns.Add("EGAIT3");
            results.Columns.Add("EGAIT5");
            results.Columns.Add("EGAIT6");
            results.Columns.Add("EGAIT7");
            results.Columns.Add("EGACAM");
            results.Columns.Add("EGVTXT");
            results.Columns.Add("EGAIT1S");
            results.Columns.Add("EGAIT3S");
            results.Columns.Add("EGAIT1R");
            results.Columns.Add("EGAIT1RM");
            results.Columns.Add("EGAIT3R");
            results.Columns.Add("EGAIT3RM");
            results.Columns.Add("EZANBR");
            results.Columns.Add("EZACAM");
            results.Columns.Add("EZFSRC");
            results.Columns.Add("T04PUNO");
            results.Columns.Add("T04PNLI");
            results.Columns.Add("T04SUNO");
            results.Columns.Add("T04SUNM");



            results.Columns.Add("T07PUNO");
            results.Columns.Add("T07PNLI");
            results.Columns.Add("T07SUNO");
            results.Columns.Add("T07SUNM");


            results.Columns.Add("T10PUNO");
            results.Columns.Add("T10PNLI");
            results.Columns.Add("T10SUNO");
            results.Columns.Add("T10SUNM");


            results.Columns.Add("EGVDSC");
            results.Columns.Add("ACAM01");
            results.Columns.Add("ACAM03");
            results.Columns.Add("ACAM03S");
            results.Columns.Add("EGTX40");
            results.Columns.Add("DATEFROM");
            results.Columns.Add("DATETO");
            results.Columns.Add("TOTALPLU");

            results.Columns.Add("T04ORQA");
            results.Columns.Add("T04PITD");
            results.Columns.Add("T04ITNO");

            results.Columns.Add("T07ORQA");
            results.Columns.Add("T07PITD");
            results.Columns.Add("T07ITNO");

            results.Columns.Add("T10ORQA");
            results.Columns.Add("T10PITD");
            results.Columns.Add("T10ITNO");
            results.Columns.Add("EGTXY1");
            results.Columns.Add("EGTXY2");
            results.Columns.Add("EGAIT4");
            results.Columns.Add("EGJSNO");

            results.Columns.Add("TXCONO");
            results.Columns.Add("TXDIVI");
            results.Columns.Add("TXJRNO");
            results.Columns.Add("TXJSNO");
            results.Columns.Add("TACAM");
            results.Columns.Add("TXVTXT");
            results.Columns.Add("EZACAMC");
            results.Columns.Add("CASEN");
            results.Columns.Add("DUPROW");

            DataRow newRow = results.NewRow();


            fromyear = datefrom.Substring(0, 4);
            frommonth = datefrom.Substring(5, 2);
            fromday = datefrom.Substring(8, 2);

            fromdate01 = fromyear + frommonth + fromday;

            toyear = dateto.Substring(0, 4);
            tomonth = dateto.Substring(5, 2);
            today = dateto.Substring(8, 2);

            todate01 = toyear + tomonth + today;



            if (ait101 == null && ait102 == null && ait103 == null && ait104 == null && ait105 == null && ait106 == null && ait107 == null && ait108 == null &&
                ait109 == null && ait110 == null && ait111 == null && ait112 == null && ait113 == null && ait114 == null && ait115 == null && ait116 == null && ait1F == null && ait1T == null)

            {
                SQLV = "" +
           "SELECT T01CONO, T01DIVI, T01JRNO,  T01ACDT, T01AIT1, T01AIT2, T01AIT3, T01AIT5, T01AIT6, T01AIT7, T01ACAM, T01VTXT , " +
           "SUM(T01ACAM) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as T01.EGAIT1_subtotal, " +
           "SUM(T01ACAM) OVER(PARTITION BY T01AIT3 order by T01AIT3) as T01.EGAIT3_subtotal, " +


       " ROW_NUMBER() over(PARTITION by T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as rntot_egait1, " +
       " MAX(rn1) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) AS 'rn_max_egait1', " +
       " ROW_NUMBER() over(PARTITION by T01AIT3 order by T01AIT3) as rntot_egait3, " +
       " MAX(rn3) OVER(PARTITION BY T01AIT3 order by T01AIT3) AS 'rn_max_egait3', EZANBR, EZACAM, EZFSRC, T04PUNO, T04PNLI, T04SUNO, T05SUNM, T04ORQA,T04PITD,T04ITNO, T07PUNO, T07PNLI,  " +
       " T08SUNO, T08SUNM, T07ORQA,T07PITD, T07ITNO, T01VDSC, EATX40, " +

         "SUM(EZACAM) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as EGAIT1_subtotal, " +
           "SUM(EZACAM) OVER(PARTITION BY T01AIT3 order by T01AIT3) as EGAIT3_subtotal, T01AIT4, T01JSNO, " +
       "TXCONO, TXDIVI, TXJRNO, TXJSNO, TXACAM, TXVTXT, " +

       " COUNT(EZACAM) OVER(PARTITION BY T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI ) as EZACAM_COUNT,  " +
       " CASE WHEN EZACAM <> TXACAM AND COUNT(TXJSNO) OVER(PARTITION BY T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI ) > 1 THEN 'TRUE' ELSE 'FALSE' END AS CASENL," +
       "" +
       " ROW_NUMBER() over(PARTITION by T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI order by T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI   ) as duprow " +

    "FROM(  " +
    "select T01.EGCONO as T01CONO, T01.EGDIVI as T01DIVI, T01.EGJRNO as T01JRNO, T01.EGJSNO as T01JSNO, T01.EGACDT as T01ACDT, T01.EGAIT1 as T01AIT1, T01.EGAIT2 as T01AIT2, T01.EGAIT3 as T01AIT3, T01.EGAIT5 as T01AIT5, T01.EGAIT6 as T01AIT6, T01.EGAIT7 as T01AIT7, T01.EGVTXT as T01VTXT, row_number() over(partition by T01.EGAIT3 " +
    "order by T01.EGAIT3) as rn3, " +
    "row_number() over(partition by T01.EGAIT3, T01.EGAIT1 order by T01.EGAIT3, T01.EGAIT1) as rn1, " +



    "T01.EGACAM as T01ACAM, T07.IBPUNO AS T07PUNO, T07.IBPNLI AS T07PNLI, T07.IBSUNO AS T07SUNO, T05.IDSUNM AS T05SUNM, " +
    "T04.IBPUNO AS T04PUNO, T04.IBPNLI AS T04PNLI, T04.IBORQA AS T04ORQA, T04.IBPITD AS T04PITD, T04.IBITNO AS T04ITNO, T04.IBSUNO AS T04SUNO, T07.IBPNLI, T02.EZACAM, T02.EZFSRC, T02.EZANBR, T02.EZSENO, " +
    "T08.IDSUNO AS T08SUNO, T07.IBORQA AS T07ORQA, T07.IBPITD AS T07PITD, T07.IBITNO AS T07ITNO, T08.IDSUNM AS T08SUNM, T01.EGVDSC AS T01VDSC, EATX40, T01.EGAIT4 AS T01AIT4, " +
    " T99.EGCONO AS TXCONO, T99.EGDIVI AS TXDIVI, T99.EGJRNO as TXJRNO, T99.EGJSNO as TXJSNO, T99.EGACAM AS TXACAM, T99.EGVTXT AS TXVTXT " +
    "from M3FGLEDG T01 " +
    "left " +
    "join M3CINACC T02 on T02.EZCONO = T01.EGCONO and T02.EZDIVI = T01.EGDIVI and T02.EZAIT1 = T01.EGAIT1 and  isnull(T02.EZAIT3, '3333') = isnull(T01.EGAIT3, '3333') and  " +
    "isnull(T02.EZAIT5, '333344') = isnull(T01.EGAIT5, '333344') and isnull(T02.EZAIT6, '333344') = isnull(T01.EGAIT6, '333344') and isnull(T02.EZAIT7, '333344') = isnull(T01.EGAIT7, '333344') and T02.EZVONO = T01.EGVONO and T02.EZYEA4 = T01.EGYEA4  " +

    "left join M3MITTRA T03 on T02.EZCONO = T03.MTCONO and T02.EZANBR = T03.MTANBR  " +
    " left join M3MPLINE T04 on T03.MTCONO = T04.IBCONO and T03.MTRIDN = T04.IBPUNO and T03.MTRIDL = T04.IBPNLI " +
    "left join M3CIDMAS T05 on T04.IBCONO = T05.IDCONO and T04.IBSUNO = T05.IDSUNO " +
    "left join CRACTR T06 on T02.EZCONO = T06.EYCONO and T02.EZANBR = T06.EYANBR  left join M3MPLINE T07 on T06.EYTRNR = T07.IBPUNO and T06.EYPONR = T07.IBPNLI left join M3CIDMAS T08 on T07.IBCONO = T08.IDCONO and " +
    "T07.IBSUNO = T08.IDSUNO " +
    " left join ZSHACCs on T01.EGCONO = EACONO and EADIVI = '0'  and T01.EGAIT1 = EAAITM  " +


      " left join M3FGLEDG T99 on T01.EGCONO = T99.EGCONO and T01.EGDIVI = T99.EGDIVI and T01.EGYEA4 = T99.EGYEA4 and T01.EGJRNO = T99.EGJRNO and T01.EGJSNO = T99.EGJSNO " +




    "where  T01.EGCONO = " + cono + "  and T01.EGDIVI =  '" + divi + "'  and " +
    " T01.EGACDT >= " + fromdate01 + " and  T01.EGACDT <=  " + todate01 + " and " +
    "(T01.EGAIT3 = '" + ait301 + "'  or " +
    " T01.EGAIT3 = '" + ait302 + "'  or " +
    " T01.EGAIT3 = '" + ait303 + "'  or " +
    " T01.EGAIT3 = '" + ait304 + "'  or " +
    " T01.EGAIT3 = '" + ait305 + "'  or " +
    " T01.EGAIT3 = '" + ait306 + "'  or " +
    " T01.EGAIT3 = '" + ait307 + "'  or " +
    " T01.EGAIT3 = '" + ait308 + "'  or " +
    " T01.EGAIT3 = '" + ait309 + "'  or " +
    " T01.EGAIT3 = '" + ait310 + "'  or " +
    " T01.EGAIT3 = '" + ait311 + "'  or " +
    " T01.EGAIT3 = '" + ait312 + "'  or " +
    " T01.EGAIT3 = '" + ait313 + "'  or " +
    " T01.EGAIT3 = '" + ait314 + "'  or " +
    " T01.EGAIT3 = '" + ait315 + "'  or " +
    " T01.EGAIT3 = '" + ait316 + "' or " +
    " (T01.EGAIT3 >= '" + ait3F + "' and  T01.EGAIT3 <= '" + ait3T + "'))  " +

        ") T  order by T01AIT3, T01AIT1, T01JRNO, T01AIT5,T01AIT6,T01AIT7, EZACAM, T04PUNO, T04PNLI, T07PUNO, T07PNLI, EZANBR, T01JSNO";
            }


            else if (ait301 == null && ait302 == null && ait303 == null && ait304 == null && ait305 == null && ait306 == null && ait307 == null && ait308 == null &&
             ait309 == null && ait310 == null && ait311 == null && ait312 == null && ait313 == null && ait314 == null && ait315 == null && ait316 == null && ait3F == null && ait3T == null)

            {
                SQLV = "" +
           "SELECT T01CONO, T01DIVI, T01JRNO,  T01ACDT, T01AIT1, T01AIT2, T01AIT3, T01AIT5, T01AIT6, T01AIT7, T01ACAM, T01VTXT , " +
           "SUM(T01ACAM) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as EGAIT1_subtotal, " +
           "SUM(T01ACAM) OVER(PARTITION BY T01AIT3 order by T01AIT3) as EGAIT3_subtotal, " +

       " ROW_NUMBER() over(PARTITION by T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as rntot_egait1, " +
       " MAX(rn1) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) AS 'rn_max_egait1', " +
       " ROW_NUMBER() over(PARTITION by T01AIT3 order by T01AIT3) as rntot_egait3, " +
       " MAX(rn3) OVER(PARTITION BY T01AIT3 order by T01AIT3) AS 'rn_max_egait3', EZANBR, EZACAM, EZFSRC, T04PUNO, T04PNLI, T04SUNO, T05SUNM, T04ORQA,T04PITD,T04ITNO, T07PUNO, T07PNLI,  " +
       " T08SUNO, T08SUNM, T07ORQA,T07PITD, T07ITNO, T01VDSC, EATX40, " +

        "SUM(EZACAM) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as EGAIT1_subtotal, " +
        "SUM(EZACAM) OVER(PARTITION BY T01AIT3 order by T01AIT3) as EGAIT3_subtotal, T01AIT4, T01JSNO, " +
        "TXCONO, TXDIVI, TXJRNO, TXJSNO, TXACAM, TXVTXT, " +
      " COUNT(EZACAM) OVER(PARTITION BY T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI ) as EZACAM_COUNT,  " +
      " CASE WHEN TXACAM <> EZACAM AND COUNT(TXJSNO) OVER(PARTITION BY T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI ) > 1 THEN 'TRUE' ELSE 'FALSE' END AS CASENL, " +

   " ROW_NUMBER() over(PARTITION by T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI order by T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI   ) as duprow " +

    "FROM(  " +
    "select T01.EGCONO AS T01CONO, T01.EGDIVI AS T01DIVI, T01.EGJRNO AS T01JRNO, T01.EGJSNO AS T01JSNO, T01.EGACDT AS T01ACDT, T01.EGAIT1 AS T01AIT1, T01.EGAIT2 AS T01AIT2, T01.EGAIT3 AS T01AIT3, T01.EGAIT5 AS T01AIT5, T01.EGAIT6 AS T01AIT6, T01.EGAIT7 AS T01AIT7, T01.EGVTXT AS T01VTXT, row_number() over(partition by T01.EGAIT3 " +
    "order by T01.EGAIT3) as rn3, " +
    "row_number() over(partition by T01.EGAIT3, T01.EGAIT1 order by T01.EGAIT3, T01.EGAIT1) as rn1, " +

    "T01.EGACAM as T01ACAM, T07.IBPUNO AS T07PUNO, T07.IBPNLI AS T07PNLI, T07.IBSUNO AS T07SUNO, T05.IDSUNM AS T05SUNM, " +
    "T04.IBPUNO AS T04PUNO, T04.IBPNLI AS T04PNLI, T04.IBORQA AS T04ORQA, T04.IBPITD AS T04PITD, T04.IBITNO AS T04ITNO, T04.IBSUNO AS T04SUNO, T07.IBPNLI, T02.EZACAM, T02.EZFSRC, T02.EZANBR, T02.EZSENO, " +
    "T08.IDSUNO AS T08SUNO, T07.IBORQA AS T07ORQA, T07.IBPITD AS T07PITD, T07.IBITNO AS T07ITNO, T08.IDSUNM AS T08SUNM, T01.EGVDSC as T01VDSC, EATX40, T01.EGAIT4 as T01AIT4, " +
     " T99.EGCONO AS TXCONO, T99.EGDIVI AS TXDIVI, T99.EGJRNO as TXJRNO, T99.EGJSNO as TXJSNO, T99.EGACAM as TXACAM, T99.EGVTXT as TXVTXT " +
    "from M3FGLEDG T01 " +
    "left " +
    "join M3CINACC T02 on T02.EZCONO = T01.EGCONO and T02.EZDIVI = T01.EGDIVI and  T02.EZAIT1 = T01.EGAIT1 and  isnull(T02.EZAIT3, '3333') = isnull(T01.EGAIT3, '3333') and  " +
    "isnull(T02.EZAIT5, '333344') = isnull(T01.EGAIT5, '333344') and isnull(T02.EZAIT6, '333344') = isnull(T01.EGAIT6, '333344') and isnull(T02.EZAIT7, '333344') = isnull(T01.EGAIT7, '333344') and T02.EZVONO = T01.EGVONO and T02.EZYEA4 = T01.EGYEA4  " +

     "left join M3MITTRA T03 on T02.EZCONO = T03.MTCONO and T02.EZANBR = T03.MTANBR " +
    " left join M3MPLINE T04 on T03.MTCONO = T04.IBCONO and T03.MTRIDN = T04.IBPUNO and T03.MTRIDL = T04.IBPNLI " +
    "left join M3CIDMAS T05 on T04.IBCONO = T05.IDCONO and T04.IBSUNO = T05.IDSUNO " +
    "left join CRACTR T06 on T02.EZCONO = T06.EYCONO and T02.EZANBR = T06.EYANBR  left join M3MPLINE T07 on T06.EYTRNR = T07.IBPUNO and T06.EYPONR = T07.IBPNLI left join M3CIDMAS T08 on T07.IBCONO = T08.IDCONO and " +
    "T07.IBSUNO = T08.IDSUNO " +
    " left join ZSHACCs on T01.EGCONO = EACONO and EADIVI = '0' and T01.EGAIT1 = EAAITM " +

    " left join M3FGLEDG T99 on T01.EGCONO = T99.EGCONO and T01.EGDIVI = T99.EGDIVI and T01.EGYEA4 = T99.EGYEA4 and T01.EGJRNO = T99.EGJRNO and T01.EGJSNO = T99.EGJSNO " +





    "where  T01.EGCONO = " + cono + "  and T01.EGDIVI =  '" + divi + "'  and " +
    " T01.EGACDT >= " + fromdate01 + " and  T01.EGACDT <= " + todate01 + " and " +
    "(T01.EGAIT1 = '" + ait101 + "'  or " +
    " T01.EGAIT1 = '" + ait102 + "'  or " +
    " T01.EGAIT1 = '" + ait103 + "'  or " +
    " T01.EGAIT1 = '" + ait104 + "'  or " +
    " T01.EGAIT1 = '" + ait105 + "'  or " +
    " T01.EGAIT1 = '" + ait106 + "'  or " +
    " T01.EGAIT1 = '" + ait107 + "'  or " +
    " T01.EGAIT1 = '" + ait108 + "'  or " +
    " T01.EGAIT1 = '" + ait109 + "'  or " +
    " T01.EGAIT1 = '" + ait110 + "'  or " +
    " T01.EGAIT1 = '" + ait111 + "'  or " +
    " T01.EGAIT1 = '" + ait112 + "'  or " +
    " T01.EGAIT1 = '" + ait113 + "'  or " +
    " T01.EGAIT1 = '" + ait114 + "'  or " +
    " T01.EGAIT1 = '" + ait115 + "'  or " +
    " T01.EGAIT1 = '" + ait116 + "' or " +
    " (T01.EGAIT1 >= '" + ait1F + "' and  T01.EGAIT1 <= '" + ait1T + "'))  " +

        ") T order by T01AIT3, T01AIT1, T01JRNO, T01AIT5,T01AIT6,T01AIT7, EZACAM, T04PUNO, T04PNLI, T07PUNO, T07PNLI, EZANBR, T01JSNO";
            }
            else
            {
                SQLV = "" +
                          "SELECT T01CONO, T01DIVI, T01JRNO,  T01ACDT, T01AIT1, T01AIT2, T01AIT3, T01AIT5, T01AIT6, T01AIT7, T01ACAM, T01VTXT , " +
                          "SUM(T01ACAM) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as EGAIT1_subtotal, " +
                          "SUM(T01ACAM) OVER(PARTITION BY T01AIT3 order by T01AIT3) as EGAIT3_subtotal, " +

                      " ROW_NUMBER() over(PARTITION by T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as rntot_egait1, " +
                      " MAX(rn1) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) AS 'rn_max_egait1', " +
                      " ROW_NUMBER() over(PARTITION by T01AIT3 order by T01AIT3) as rntot_egait3, " +
                      " MAX(rn3) OVER(PARTITION BY T01AIT3 order by T01AIT3) AS 'rn_max_egait3', EZANBR, EZACAM, EZFSRC, T04PUNO, T04PNLI, T04SUNO, T05SUNM, T04ORQA,T04PITD,T04ITNO, T07PUNO, T07PNLI,  " +
                      " T08SUNO, T08SUNM, T07ORQA,T07PITD, T07ITNO, T01VDSC, EATX40,  " +

                      " SUM(EZACAM) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as EZAIT1_subtotal, " +
                      " SUM(EZACAM) OVER(PARTITION BY T01AIT3, T01AIT1 order by T01AIT3, T01AIT1) as EZAIT1_subtotal, T01AIT4, T01JSNO, " +
                    "TXCONO, TXDIVI, TXJRNO, TXJSNO, TXACAM, TXVTXT, " +
                        " COUNT(EZACAM) OVER(PARTITION BY T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI ) as EZACAM_COUNT,  " +
                         " CASE WHEN EZACAM <> TXACAM AND COUNT(TXJSNO) OVER(PARTITION BY T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI ) > 1 THEN 'TRUE' ELSE 'FALSE' END AS CASENL, " +

            " ROW_NUMBER() over(PARTITION by T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI order by T01AIT1,T01AIT2,T01AIT3,T01AIT4,T01AIT5,T01AIT6,T01AIT7,T04PUNO,T07PUNO,T04PNLI,T07PNLI   ) as duprow " +

                      "FROM(  " +
                   "select T01.EGCONO as T01CONO, T01.EGDIVI as T01DIVI, T01.EGJRNO as T01JRNO, T01.EGJSNO as T01JSNO, T01.EGACDT as T01ACDT, T01.EGAIT1 as T01AIT1, T01.EGAIT2 as T01AIT2, T01.EGAIT3 as T01AIT3, T01.EGAIT5 as T01AIT5, T01.EGAIT6 as T01AIT6, T01.EGAIT7 as T01AIT7, T01.EGVTXT as T01VTXT, row_number() over(partition by T01.EGAIT3 " +
                   "order by T01.EGAIT3) as rn3, " +
                   "row_number() over(partition by T01.EGAIT3, T01.EGAIT1 order by T01.EGAIT3, T01.EGAIT1) as rn1, " +

                   "T01.EGACAM as T01ACAM, T07.IBPUNO AS T07PUNO, T07.IBPNLI AS T07PNLI, T07.IBSUNO AS T07SUNO, T05.IDSUNM AS T05SUNM, " +
                   "T04.IBPUNO AS T04PUNO, T04.IBPNLI AS T04PNLI, T04.IBORQA AS T04ORQA, T04.IBPITD AS T04PITD, T04.IBITNO AS T04ITNO, T04.IBSUNO AS T04SUNO, T07.IBPNLI, T02.EZACAM, T02.EZFSRC, T02.EZANBR, T02.EZSENO, " +
                   "T08.IDSUNO AS T08SUNO, T07.IBORQA AS T07ORQA, T07.IBPITD AS T07PITD, T07.IBITNO AS T07ITNO, T08.IDSUNM AS T08SUNM, T01.EGVDSC as T01VDSC, EATX40, T01.EGAIT4 as T01AIT4, " +
                    " T99.EGCONO AS TXCONO, T99.EGDIVI AS TXDIVI, T99.EGJRNO as TXJRNO, T99.EGJSNO as TXJSNO, T99.EGACAM as TXACAM, T99.EGVTXT as TXVTXT " +
                   "from M3FGLEDG T01 " +
                   "left " +
                   "join M3CINACC T02 on T02.EZCONO = T01.EGCONO and T02.EZDIVI = T01.EGDIVI and T02.EZAIT1 = T01.EGAIT1 and  isnull(T02.EZAIT3, '3333') = isnull(T01.EGAIT3, '3333') and  " +
                   "isnull(T02.EZAIT5, '333344') = isnull(T01.EGAIT5, '333344') and isnull(T02.EZAIT6, '333344') = isnull(T01.EGAIT6, '333344') and isnull(T02.EZAIT7, '333344') = isnull(T01.EGAIT7, '333344') and T02.EZVONO = T01.EGVONO and T02.EZYEA4 = T01.EGYEA4  " +

                   "left join M3MITTRA T03 on T02.EZCONO = T03.MTCONO and T02.EZANBR = T03.MTANBR" +
                   " left join M3MPLINE T04 on T03.MTCONO = T04.IBCONO and T03.MTRIDN = T04.IBPUNO and T03.MTRIDL = T04.IBPNLI " +
                   "left join M3CIDMAS T05 on T04.IBCONO = T05.IDCONO and T04.IBSUNO = T05.IDSUNO " +
                   "left join CRACTR T06 on T02.EZCONO = T06.EYCONO and T02.EZANBR = T06.EYANBR  left join M3MPLINE T07 on T06.EYTRNR = T07.IBPUNO and T06.EYPONR = T07.IBPNLI left join M3CIDMAS T08 on T07.IBCONO = T08.IDCONO and " +
                   "T07.IBSUNO = T08.IDSUNO " +
                   " left join ZSHACCs on T01.EGCONO = EACONO and EADIVI = '0' and T01.EGAIT1 = EAAITM " +
                   " left join M3FGLEDG T99 on T01.EGCONO = T99.EGCONO and T01.EGDIVI = T99.EGDIVI and T01.EGYEA4 = T99.EGYEA4 and T01.EGJRNO = T99.EGJRNO and T01.EGJSNO = T99.EGJSNO " +
                   "where  T01.EGCONO = " + cono + "  and T01.EGDIVI =  '" + divi + "'  and " +
 "T01.EGACDT >= " + fromdate01 + " and  T01.EGACDT <= " + todate01 + " and  " +
  "(T01.EGAIT3 = '" + ait301 + "'  or " +
  " T01.EGAIT3 = '" + ait302 + "'  or " +
  " T01.EGAIT3 = '" + ait303 + "'  or " +
  " T01.EGAIT3 = '" + ait304 + "'  or " +
  " T01.EGAIT3 = '" + ait305 + "'  or " +
  " T01.EGAIT3 = '" + ait306 + "'  or " +
  " T01.EGAIT3 = '" + ait307 + "'  or " +
  " T01.EGAIT3 = '" + ait308 + "'  or " +
  " T01.EGAIT3 = '" + ait309 + "'  or " +
  " T01.EGAIT3 = '" + ait310 + "'  or " +
  " T01.EGAIT3 = '" + ait311 + "'  or " +
  " T01.EGAIT3 = '" + ait312 + "'  or " +
  " T01.EGAIT3 = '" + ait313 + "'  or " +
  " T01.EGAIT3 = '" + ait314 + "'  or " +
  " T01.EGAIT3 = '" + ait315 + "'  or " +
  " T01.EGAIT3 = '" + ait316 + "' or " +
  " (T01.EGAIT3 >= '" + ait3F + "' and  T01.EGAIT3 <= '" + ait3T + "')) and  " +
  " (T01.EGAIT1 = '" + ait101 + "' or " +
  " T01.EGAIT1 = '" + ait102 + "'  or " +
  " T01.EGAIT1 = '" + ait103 + "'  or " +
  " T01.EGAIT1 = '" + ait104 + "'  or " +
  " T01.EGAIT1 = '" + ait105 + "'  or " +
  " T01.EGAIT1 = '" + ait106 + "'  or " +
  " T01.EGAIT1 = '" + ait107 + "'  or " +
  " T01.EGAIT1 = '" + ait108 + "'  or " +
  " T01.EGAIT1 = '" + ait109 + "'  or " +
  " T01.EGAIT1 = '" + ait110 + "'  or " +
  " T01.EGAIT1 = '" + ait111 + "'  or " +
  " T01.EGAIT1 = '" + ait112 + "'  or " +
  " T01.EGAIT1 = '" + ait113 + "'  or " +
  " T01.EGAIT1 = '" + ait114 + "'  or " +
  " T01.EGAIT1 = '" + ait115 + "'  or " +
  " T01.EGAIT1 = '" + ait116 + "'  or " +
  " (T01.EGAIT1 >= '" + ait1F + "' and  T01.EGAIT1 <= '" + ait1T + "'))  " +
               ") T  order by T01AIT3, T01AIT1, T01JRNO, T01AIT5,T01AIT6,T01AIT7, EZACAM, T04PUNO, T04PNLI, T07PUNO, T07PNLI, EZANBR, T01JSNO";



            }



            totalplu = "PLU - ";


            using (ApplicationDB db = this.CreateApplicationDB())
            {


                IDbCommand sqlCommand = db.CreateCommand();

                sqlCommand.CommandText = SQLV;


                sqlCommand.CommandType = System.Data.CommandType.Text;
                IDataReader dr = sqlCommand.ExecuteReader();

                // saved values

                while (dr.Read())


                {




                    egcono = dr[0].ToString();
                    egdivi = dr[1].ToString();
                    egjrno = dr[2].ToString();
                    egacdt = dr[3].ToString();
                    egait1 = dr[4].ToString();
                    egait2 = dr[5].ToString();
                    egait3 = dr[6].ToString();
                    egait5 = dr[7].ToString();
                    egait6 = dr[8].ToString();
                    egait7 = dr[9].ToString();
                    egacam = dr[10].ToString();
                    egvtxt = dr[11].ToString();
                    egait1s = dr[12].ToString();
                    egait3s = dr[13].ToString();
                    egait1r = dr[14].ToString();
                    egait1rm = dr[15].ToString();
                    egait3r = dr[16].ToString();
                    egait3rm = dr[17].ToString();
                    ezanbr = dr[18].ToString();
                    ezacam = dr[19].ToString();
                    ezfsrc = dr[20].ToString();
                    t04puno = dr[21].ToString();
                    t04pnli = dr[22].ToString();
                    t04suno = dr[23].ToString();
                    t04sunm = dr[24].ToString();
                    t04orqa = dr[25].ToString();
                    t04pitd = dr[26].ToString();
                    t04itno = dr[27].ToString();


                    t07puno = dr[28].ToString();
                    t07pnli = dr[29].ToString();
                    t07suno = dr[30].ToString();
                    t07sunm = dr[31].ToString();
                    t07orqa = dr[32].ToString();
                    t07pitd = dr[33].ToString();
                    t07itno = dr[34].ToString();

                    egvdsc = dr[35].ToString();
                    egtx40 = dr[36].ToString();
                    egait4 = dr[39].ToString();
                    egjsno = dr[40].ToString();

                    tcono = dr[41].ToString();
                    tdivi = dr[42].ToString();
                    tjrno = dr[43].ToString();
                    tjsno = dr[44].ToString();
                    tacam = dr[45].ToString();
                    tvtxt = dr[46].ToString();
                    ezacamc = dr[47].ToString();
                    casen = dr[48].ToString();

                    duprow = dr[49].ToString();

                    t10puno = null; t10pnli = null; t10suno = null; t10sunm = null; t10orqa = null; t10pitd = null; t10itno = null;

                    if (ezfsrc == "1") { t10puno = t04puno; t10pnli = t04pnli; t10suno = t04suno; t10sunm = t04sunm; t10orqa = t04orqa; t10pitd = t04pitd; t10itno = t04itno; egacam = ezacam; }
                    if (ezfsrc == "3") { t10puno = t07puno; t10pnli = t07pnli; t10suno = t07suno; t10sunm = t07sunm; t10orqa = t07orqa; t10pitd = t07pitd; t10itno = t07itno; egacam = ezacam; }

                    string totalplu10 = totalplu + egait3;

                    dupchk = "0";

                    casen = "0";


                    if (egait1 == egait1sav && egait3 == egait3sav && egacam == egacamsav && egait5 == egait5sav && egait6 == egait6sav && egait7 == egait7sav && t10puno == t10punosav && t10pnli == t10pnlisav && ezanbr == ezanbrsav) { dupchk = "1"; casen = "1"; }


                    if (egvdsc != "COSTACC") { dupchk = "0"; }


                    //     if (tjrno == tjrnosav && tjsno == tjsnosav && tjrnosav != null) { dupchk = "1"; }



                    if (dupchk == "0")
                    {

                        if (egait1 != egait1sav) { egtxy1 = 0; }
                        if (egait3 != egait3sav)
                        {
                            egtxy2 = 0; egtxy1 = 0; newRow["EGCONO"] = "blanks"; results.Rows.Add(newRow); newRow = results.NewRow();
                        }


                        decimal val01 = Convert.ToDecimal(egacam);
                        egtxy1 = egtxy1 + val01;
                        egtxy2 = egtxy2 + val01;
                    }


                    string egtxy1s = egtxy1.ToString();
                    string egtxy2s = egtxy2.ToString();

                    if (dupchk == "0")
                    {

                        newRow["EGCONO"] = egcono;
                        newRow["EGDIVI"] = egdivi;
                        newRow["EGJRNO"] = egjrno;
                        newRow["EGACDT"] = egacdt;
                        newRow["EGAIT1"] = egait1;
                        newRow["EGAIT2"] = egait2;
                        newRow["EGAIT3"] = egait3;
                        newRow["EGAIT5"] = egait5;
                        newRow["EGAIT6"] = egait6;
                        newRow["EGAIT7"] = egait7;

                        newRow["EGVTXT"] = egvtxt;
                        newRow["EGAIT1S"] = egait1s;
                        newRow["EGAIT3S"] = egait3s;
                        newRow["EGAIT1R"] = egait1rx;
                        newRow["EGAIT1RM"] = egait1rm;
                        newRow["EGAIT3R"] = egait3rx;
                        newRow["EGAIT3RM"] = egait3rm;
                        newRow["EZANBR"] = ezanbr;
                        newRow["EZACAM"] = ezacam;
                        newRow["EZFSRC"] = ezfsrc;
                        newRow["T04PUNO"] = t04puno;
                        newRow["T04PNLI"] = t04pnli;
                        newRow["T04SUNO"] = t04suno;
                        newRow["T04SUNM"] = t04sunm;
                        newRow["T07PUNO"] = t07puno;
                        newRow["T07PNLI"] = t07pnli;
                        newRow["T07SUNO"] = t07suno;
                        newRow["T07SUNM"] = t07sunm;

                        newRow["EGACAM"] = egacam;
                        newRow["T10PUNO"] = t10puno;
                        newRow["T10PNLI"] = t10pnli;
                        newRow["T10SUNO"] = t10suno;
                        newRow["T10SUNM"] = t10sunm;

                        newRow["EGVDSC"] = egvdsc;
                        newRow["ACAM01"] = divi;
                        newRow["ACAM03"] = acam003;
                        newRow["ACAM03S"] = SQLV;
                        newRow["EGTX40"] = egtx40;
                        newRow["DATEFROM"] = fromdate01;
                        newRow["DATETO"] = todate01;
                        newRow["TOTALPLU"] = totalplu10;

                        newRow["T04ORQA"] = t04orqa;
                        newRow["T04PITD"] = t04pitd;
                        newRow["T04ITNO"] = t04itno;

                        newRow["T07ORQA"] = t07orqa;
                        newRow["T07PITD"] = t07pitd;
                        newRow["T07ITNO"] = t07itno;

                        newRow["T10ORQA"] = t10orqa;
                        newRow["T10PITD"] = t10pitd;
                        newRow["T10ITNO"] = t10itno;

                        newRow["EGTXY1"] = egtxy1s;
                        newRow["EGTXY2"] = egtxy2s;
                        newRow["EGAIT4"] = egait4;
                        newRow["EGJSNO"] = egjsno;

                        newRow["TXCONO"] = tcono;
                        newRow["TXDIVI"] = tdivi;
                        newRow["TXJRNO"] = tjrno;
                        newRow["TXJSNO"] = tjsno;
                        newRow["TACAM"] = tacam;
                        newRow["TXVTXT"] = tvtxt;
                        newRow["EZACAMC"] = ezacamc;
                        newRow["CASEN"] = casen;
                        newRow["DUPROW"] = duprow;


                        results.Rows.Add(newRow);
                        newRow = results.NewRow();

                        egait1sav = egait1;
                        egait2sav = egait2;
                        egait3sav = egait3;
                        egait5sav = egait5;
                        egait6sav = egait6;
                        egait7sav = egait7;
                        egacamsav = egacam;
                        ezanbrsav = ezanbr;

                        t10punosav = t10puno;
                        t10pnlisav = t10pnli;

                        egpunosav = t10puno;
                        egpnlisav = t10pnli;

                        tjrnosav = tjrno;
                        tjsnosav = tjsno;




                    }



                }



                dr.Close();
                dr = null;




                return results.CreateDataReader();

            }



        }











    }


}

