﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Mongoose.IDO;
using Mongoose.IDO.Protocol;
using Mongoose.IDO.DataAccess;
using Mongoose.Core.Common;

namespace CA002EEXT
{
    public class CA002EEXT : IDOExtensionClass
    {

        string SAV_PUNO;
        string SAV_PNLI;
        string SAV_SUNO;
        string SAV_SUNM;
        string SAV_ORQA;
        string SAV_PITD;
        string SAV_ITNO;

        string SAV_PUNO2;
        string SAV_PNLI2;
        string SAV_SUNO2;
        string SAV_SUNM2;
        string SAV_ORQA2;
        string SAV_PITD2;
        string SAV_ITNO2;
        string dupcheck;


        public override void SetContext(IIDOExtensionClassContext context)
        {
            base.SetContext(context);
            Context.IDO.PostLoadCollection += new IDOEventHandler(IDO_PostLoadCollection);
        }

        void IDO_PostLoadCollection(object sender, IDOEventArgs args)
        {
            LoadCollectionResponseData responseData = args.ResponsePayload as LoadCollectionResponseData;

        }


        public IDataReader App01(int cono, int divi, int yea4, int jrno, int jsno)

        {
            DataTable results = new DataTable("CA002");





            results.Columns.Add("IBPUNO");
            results.Columns.Add("IBPNLI");
            results.Columns.Add("IBSUNO");
            results.Columns.Add("IDSUNM");
            results.Columns.Add("EZACAM");
            results.Columns.Add("IBORQA");
            results.Columns.Add("IBPITD");
            results.Columns.Add("IBITNO");
            results.Columns.Add("EGAIT1");
            results.Columns.Add("EGAIT2");
            results.Columns.Add("EGAIT3");
            results.Columns.Add("EGAIT4");
            results.Columns.Add("EGAIT5");
            results.Columns.Add("EGAIT6");
            results.Columns.Add("EGAIT7");



            DataRow newRow = results.NewRow();

            using (ApplicationDB db = this.CreateApplicationDB())
            {


                IDbCommand sqlCommand = db.CreateCommand();

                sqlCommand.CommandText = string.Format("select EZFSRC, T01.IBPUNO, T01.IBPNLI, T05.IDSUNO, T05.IDSUNM, T01.IBORQA, T01.IBPITD, T01.IBITNO, T02.IBPUNO, T02.IBPNLI, T03.IDSUNO, T03.IDSUNM, T02.IBORQA, T02.IBPITD, T02.IBITNO, EZACAM," +
                    " EGAIT1, EGAIT2, EGAIT3, EGAIT4, EGAIT5, EGAIT6, EGAIT7 FROM M3CINACC " +
                    " left join M3FGLEDG on EZCONO = EGCONO and EZDIVI = EGDIVI and ISNULL(EZAIT1,'') =  ISNULL(EGAIT1,'') and ISNULL(EZAIT2,'') = ISNULL(EGAIT2,'') and ISNULL(EZAIT3,'') = ISNULL(EGAIT3,'') and ISNULL(EZAIT4,'') = ISNULL(EGAIT4,'') and ISNULL(EZAIT5,'') = ISNULL(EGAIT5,'') and ISNULL(EZAIT6,'') = ISNULL(EGAIT6,'') and ISNULL(EZAIT7,'') = ISNULL(EGAIT7,'') and EZVONO = EGVONO" +
                    " left join M3MITTRA on EZCONO = MTCONO and EZANBR = MTANBR  left join M3MPLINE T01 on MTCONO = T01.IBCONO and MTRIDN = T01.IBPUNO and MTRIDL = T01.IBPNLI left join M3CIDMAS T05 on T01.IBCONO = T05.IDCONO and T01.IBSUNO = T05.IDSUNO " +
                    " left join CRACTR on EZCONO = EYCONO and EZANBR = EYANBR left join M3MPLINE T02 on EYCONO = T02.IBCONO and EYTRNR = T02.IBPUNO and EYPONR = T02.IBPNLI and EYPOSX = T02.IBPNLS left join M3CIDMAS T03 on T02.IBCONO = T03.IDCONO and T02.IBSUNO = T03.IDSUNO " +
                    " where EZCONO = " + cono + " and EZDIVI = '" + divi + "' and EGYEA4 = " + yea4 + " and EGJRNO = " + jrno + " and EGJSNO = " + jsno + " order by EGCONO, EGDIVI, T01.IBPUNO, T01.IBPNLI, T02.IBPUNO, T02.IBPNLI");
                sqlCommand.CommandType = System.Data.CommandType.Text;

                IDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())

                {

                    string FSRC = dr[0].ToString();
                    string PUNO = dr[1].ToString();
                    string PNLI = dr[2].ToString();
                    string SUNO = dr[3].ToString();
                    string SUNM = dr[4].ToString();
                    string ORQA = dr[5].ToString();
                    string PITD = dr[6].ToString();
                    string ITNO = dr[7].ToString();

                    string PUNO2 = dr[8].ToString();
                    string PNLI2 = dr[9].ToString();
                    string SUNO2 = dr[10].ToString();
                    string SUNM2 = dr[11].ToString();
                    string ORQA2 = dr[12].ToString();
                    string PITD2 = dr[13].ToString();
                    string ITNO2 = dr[14].ToString();

                    string ACAM = dr[15].ToString();

                    string AIT1 = dr[16].ToString();
                    string AIT2 = dr[17].ToString();
                    string AIT3 = dr[18].ToString();
                    string AIT4 = dr[19].ToString();
                    string AIT5 = dr[20].ToString();
                    string AIT6 = dr[21].ToString();
                    string AIT7 = dr[22].ToString();





                    if (FSRC == "1")
                    {
                        newRow["IBPUNO"] = PUNO;
                        newRow["IBPNLI"] = PNLI;
                        newRow["IBSUNO"] = SUNO;
                        newRow["IDSUNM"] = SUNM;
                        newRow["IBORQA"] = ORQA;
                        newRow["IBPITD"] = PITD;
                        newRow["IBITNO"] = ITNO;


                        if (PUNO == SAV_PUNO && PNLI == SAV_PNLI && SAV_PUNO != null) { dupcheck = "TRUE"; }
                    //     if (ACAM != acam01) { dupcheck = "TRUE"; }

                    }

                    if (FSRC == "3")
                    {
                        newRow["IBPUNO"] = PUNO2;
                        newRow["IBPNLI"] = PNLI2;
                        newRow["IBSUNO"] = SUNO2;
                        newRow["IDSUNM"] = SUNM2;
                        newRow["IBORQA"] = ORQA2;
                        newRow["IBPITD"] = PITD2;
                        newRow["IBITNO"] = ITNO2;

                        if (PUNO2 == SAV_PUNO2 && PNLI2 == SAV_PNLI2 && SAV_PUNO2 != null) { dupcheck = "TRUE"; }
                  //      if (ACAM != acam01) { dupcheck = "TRUE"; }

                    }


                    newRow["EZACAM"] = ACAM;

                    newRow["EGAIT1"] = AIT1;
                    newRow["EGAIT2"] = AIT2;
                    newRow["EGAIT3"] = AIT3;
                    newRow["EGAIT4"] = AIT4;
                    newRow["EGAIT5"] = AIT5;
                    newRow["EGAIT6"] = AIT6;
                    newRow["EGAIT7"] = AIT7;


                    if (dupcheck != "TRUE")
                    {

                        results.Rows.Add(newRow);
                        newRow = results.NewRow();
                    }

                    dupcheck = "FALSE";

                    SAV_PUNO = PUNO;
                    SAV_PNLI = PNLI;
                    SAV_SUNO = SUNO;
                    SAV_SUNM = SUNM;
                    SAV_ORQA = ORQA;
                    SAV_PITD = PITD;
                    SAV_ITNO = ITNO;

                    SAV_PUNO2 = PUNO2;
                    SAV_PNLI2 = PNLI2;
                    SAV_SUNO2 = SUNO2;
                    SAV_SUNM2 = SUNM2;
                    SAV_ORQA2 = ORQA2;
                    SAV_PITD2 = PITD2;
                    SAV_ITNO2 = ITNO2;



                }




                return results.CreateDataReader();

            }



        }









    }


}

