﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Mongoose.IDO;
using Mongoose.IDO.Protocol;
using Mongoose.IDO.DataAccess;
using Mongoose.Core.Common;

namespace CA001B
{
    public class CA001B : IDOExtensionClass
    {

        public override void SetContext(IIDOExtensionClassContext context)
        {
            base.SetContext(context);
            Context.IDO.PostLoadCollection += new IDOEventHandler(IDO_PostLoadCollection);
        }

        void IDO_PostLoadCollection(object sender, IDOEventArgs args)
        {
            LoadCollectionResponseData responseData = args.ResponsePayload as LoadCollectionResponseData;

        }


        public IDataReader App01(String Input1, String Input2, String Input3, String Input4)

        {
            DataTable results = new DataTable("CA001");

            results.Columns.Add("EGAIT1");
            results.Columns.Add("EGAIT2");
            results.Columns.Add("EGAIT3");
            results.Columns.Add("EGAIT5");
            results.Columns.Add("EGAIT6");
            results.Columns.Add("EGAIT7");
            results.Columns.Add("EGACAM");
            results.Columns.Add("EGAIT4");

            DataRow newRow = results.NewRow();



            using (ApplicationDB db = this.CreateApplicationDB())
            {

                if (string.IsNullOrEmpty(Input3))
                {
                    Input3 = "000";
                }

                if (string.IsNullOrEmpty(Input4))
                {
                    Input4 = "000";
                }


                IDbCommand sqlCommand = db.CreateCommand();

                sqlCommand.CommandText = string.Format("select EGAIT1,EGAIT2,EGAIT3,EGAIT5,EGAIT6,EGAIT7,EGACAM,EGAIT4 FROM M3FGLEDG" +
                    " where EGCONO = " + Input3 + " and EGDIVI = '" + Input4 + "' and EGAIT1 = '" + Input1 + "' and EGVONO = " + Input2 + " order by EGAIT1, EGAIT2, EGAIT3, EGAIT5, EGAIT6");
                sqlCommand.CommandType = System.Data.CommandType.Text;

                IDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())

                {
                    string dim1 = dr[0].ToString();
                    string dim2 = dr[1].ToString();
                    string dim3 = dr[2].ToString();
                    string dim5 = dr[3].ToString();
                    string dim6 = dr[4].ToString();
                    string dim7 = dr[5].ToString();
                    string amnt = dr[6].ToString();
                    string dim4 = dr[7].ToString();



                    newRow["EGAIT1"] = dim1;
                    newRow["EGAIT2"] = dim2;
                    newRow["EGAIT3"] = dim3;
                    newRow["EGAIT5"] = dim5;
                    newRow["EGAIT6"] = dim6;
                    newRow["EGAIT7"] = dim7;
                    newRow["EGACAM"] = amnt;
                    newRow["EGAIT4"] = dim4;


                    results.Rows.Add(newRow);
                    newRow = results.NewRow();

                }




                return results.CreateDataReader();

            }



        }


        public IDataReader App02(String Input1, String Input2, int Input3)

        {
            DataTable results = new DataTable("CA002h");

            results.Columns.Add("EGAIT1");
            results.Columns.Add("EGAIT2");
            results.Columns.Add("EGAIT3");
            results.Columns.Add("EGAIT5");
            results.Columns.Add("EGAIT6");
            results.Columns.Add("EGACAM");


            DataRow newRow = results.NewRow();

            using (ApplicationDB db = this.CreateApplicationDB())
            {


                IDbCommand sqlCommand = db.CreateCommand();

                sqlCommand.CommandText = string.Format("select EGAIT1,EGAIT2,EGAIT3,EGAIT5,EGAIT6,EGACAM FROM M3FGLEDG where EGAIT1 = '" + Input1 + "' and EGVONO = " + Input2);
                sqlCommand.CommandType = System.Data.CommandType.Text;

                IDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())

                {
                    string dim1 = dr[0].ToString();
                    string dim2 = dr[1].ToString();
                    string dim3 = dr[2].ToString();
                    string dim5 = dr[3].ToString();
                    string dim6 = dr[4].ToString();
                    string amnt = dr[5].ToString();

                    newRow["EGAIT1"] = dim1;
                    newRow["EGAIT2"] = dim2;
                    newRow["EGAIT3"] = dim3;
                    newRow["EGAIT5"] = dim5;
                    newRow["EGAIT6"] = dim6;
                    newRow["EGACAM"] = amnt;


                    results.Rows.Add(newRow);
                    newRow = results.NewRow();

                }




                return results.CreateDataReader();

            }



        }








    }


}

